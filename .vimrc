" All system-wide defaults are set in $VIMRUNTIME/archlinux.vim (usually just
" /usr/share/vim/vimfiles/archlinux.vim) and sourced by the call to :runtime
" you can find below.  If you wish to change any of those settings, you should
" do it in this file (/etc/vimrc), since archlinux.vim will be overwritten
" everytime an upgrade of the vim packages is performed.  It is recommended to
" make changes after sourcing archlinux.vim since it alters the value of the
" 'compatible' option.

" This line should not be removed as it ensures that various options are
" properly set to work with the Vim-related packages.
runtime! archlinux.vim

" If you prefer the old-style vim functionalty, add 'runtime! vimrc_example.vim'
" Or better yet, read /usr/share/vim/vim80/vimrc_example.vim or the vim manual
" and configure vim to your own liking!

" do not load defaults if ~/.vimrc is missing
"let skip_defaults_vim=1

"---------**my keybindings:**--------
"ESC is set to jk
"Undotree toggle is set to F5
"Spellcheck toggle is set to F6
"Leaderkey is set to space
"
""map spellcheck onto F6
map <F6> :setlocal spell! spelllang=en_gb<CR> 

"map ESC onto ii 
inoremap jk <ESC>

" map leaderkey (commandkey) onto spacebar
let mapleader = " "

"map toggle undotree onto F5
nnoremap <F5> :UndotreeToggle<CR>

"map goyo onto leader g
map <leader>g :Goyo <CR>

" map limelight
map <leader>l :Limelight!! <CR>

"----vifm keybindings---
map <leader>vv :Vifm<CR>
map <leader>vs :VsplitVifm<CR>
map <leader>sp :SplitVifm<CR>
map <leader>dv :DiffVifm<CR>
map <leader>tv :TabVifm<CR>

" ----------**keybindings end**--------

"----------**plugins**----------
"use plugin manager plug https://github.com/junegunn/vim-plug 
call plug#begin('~/.vim/plugged')
Plug 'sainnhe/gruvbox-material'
Plug 'morhetz/gruvbox' 
Plug 'mbbill/undotree'
Plug 'junegunn/goyo.vim'
Plug 'junegunn/limelight.vim'
Plug 'itchyny/lightline.vim'
Plug 'vifm/vifm.vim'

call plug#end()
"end of plugin calls
"-------------**plugins end**--------

"------------**setup undo strategy**---

set noswapfile "stop saving temp files all over dir
set nobackup "remove auto backup
set undofile

"setup undo dir for persistent undo to use as cache
set undodir=~/.vim/undodir
" setup persistent undo by storing undo cache in dir setup in set undodir
if has("persistent_undo")
    set undodir=$HOME."/.undodir"
    set undofile
endif
"----------**end undo setup**-------------

"----------**setup interface**----------
syntax on "highilght syntax

set noerrorbells

"  set number relativenumber "show line numbers - relative
set number


set background=dark

let g:gruvbox_contrast_dark = 'hard'
let g:gruvbox_material_transparent_background = 0
let g:gruvbox_material_diagnostic_text_highlight = 0

colorscheme gruvbox-material 
 
" setup lightline statusbar
set laststatus=2

" changes the shading of the line where the cursor sits so you can see it
set cursorline

"--------------**end interface setup**-----
"
"----------**setup search characteristics**------
set hlsearch "highlight all results

set ignorecase "ignore case in search

set incsearch "show search results as you type

"-----------**end search settings**-------

"-----------**limelight settings**-----
let g:limelight_conceal_ctermfg = 'black'

" Default: 0.5
let g:limelight_default_coefficient = 0.7

" Number of preceding/following paragraphs to include (default: 0)
let g:limelight_paragraph_span = 1

" Beginning/end of paragraph
"   When there's no empty line between the paragraphs
"   and each paragraph starts with indentation
let g:limelight_bop = '^\s'
let g:limelight_eop = '\ze\n^\s'

" Highlighting priority (default: 10)
"   Set it to -1 not to overrule hlsearch
let g:limelight_priority = -1

"-----------------** end of limelight settings ** ---

" Goyo and Limelight working together
autocmd! User GoyoEnter Limelight
autocmd! User GoyoLeave Limelight!

