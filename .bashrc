# .bashrc

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '

# neofetch

export PATH="$HOME/.emacs.d/bin:$PATH"

# Turn off system beep in console:
xset b off
xset b 0 0 0

fish


alias config='/usr/bin/git --git-dir=/home/dave/.dwmBare --work-tree=/home/dave' 
